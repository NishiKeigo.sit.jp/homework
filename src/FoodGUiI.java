import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUiI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane list;
    private JButton gyozaButton;
    private JButton karaageButton;
    private JButton yakisobaButton;
    private JButton checkOutButton;
    private JTextField total;
    private JButton ALLButton;
    int sum = 0;


    void order(String food,String cresit, int num){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food ,
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            String currentText = list.getText();
            list.setText(currentText + food + "  " +cresit+" \n");

            JOptionPane.showMessageDialog(null,
                    "Order for " + food + " received." );

            list.getText();
            sum = sum + num;
            total.setText("Total" + sum + "yen");
        }
    }
    public FoodGUiI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            order("Tempura","600yen",600);


            }
        });
        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("tempura.jpg")
        ));

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Ramen","500yen",500);
            }
        });
        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.jpg")
        ));

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon","300yen",300);
            }
        });
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("udon.jpeg")
        ));

        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza","400yen",400);
            }
        });
        gyozaButton.setIcon(new ImageIcon(this.getClass().getResource("gyoza.jpg")
        ));

        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage","200yen",200);
            }
        });
        karaageButton.setIcon(new ImageIcon(this.getClass().getResource("karaage.jpg")
        ));

        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba","450yen",450);
            }
        });
        yakisobaButton.setIcon(new ImageIcon(this.getClass().getResource("yakisoba.jpg")
        ));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog
                        (null,
                                "Do you finish your oreder?",
                                "*Last Confirmation!*",
                                JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you!");
                    list.setText("");
                    total.setText("Total 0 yen" );
                    sum = 0;
                }
            }
        });
        ALLButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Do you want to cancel your order?"  ,
                        "Order Cancel!",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0){
                    list.setText("");
                    total.setText("Total 0 yen" );
                    sum = 0;
                }else{
                    ;
                }

            }
        });
    }







    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUiI");
        frame.setContentPane(new FoodGUiI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
